#!/system/bin/sh

echo -e "Dragon Terminal Colors"
echo -ne "\t\e[30m    Dim black   \e[0m\n"
echo -ne "\t\e[31m    Dim red     \e[0m\n"
echo -ne "\t\e[32m    Dim green   \e[0m\n"
echo -ne "\t\e[33m    Dim yellow  \e[0m\n"
echo -ne "\t\e[34m    Dim blue    \e[0m\n"
echo -ne "\t\e[35m    Dim magenta \e[0m\n"
echo -ne "\t\e[36m    Dim cyan    \e[0m\n"
echo -ne "\t\e[37m    Dim white   \e[0m\n"
echo -ne "\t\e[1;30m    Bright black  \e[0m\n"
echo -ne "\t\e[1;31m    Bright red    \e[0m\n"
echo -ne "\t\e[1;32m    Bright green  \e[0m\n"
echo -ne "\t\e[1;33m    Bright yellow \e[0m\n"
echo -ne "\t\e[1;34m    Bright blue   \e[0m\n"
echo -ne "\t\e[1;35m    Bright magenta\e[0m\n"
echo -ne "\t\e[1;36m    Bright cyan   \e[0m\n"
echo -ne "\t\e[1;37m    Bright white  \e[0m\n"

sleep 01

exit 0